#!/bin/sh

fetch() {
  if [ $LOCAL = false ]; then
    mkdir .tmp
    git clone $FRONTEND_REPO --branch $BRANCH --depth 1 .tmp/fe
    git clone $LEGACY_REPO --branch master --depth 1 .tmp/legacy
    git clone $MIGRATOR_REPO --branch master --depth 1 .tmp/migrator
    FRONTEND_REPO=.tmp/fe
    LEGACY_REPO=.tmp/legacy
    MIGRATOR_REPO=.tmp/migrator
  fi
  rm -rf docs/frontend
  rm -rf docs/legacy
  rm -rf docs/migrator
  mkdir docs/frontend
  mkdir docs/legacy
  mkdir docs/migrator
  cp -r $FRONTEND_REPO/docs/* docs/frontend
  cp -r $LEGACY_REPO/docs/* docs/legacy
  cp -r $MIGRATOR_REPO/docs/* docs/migrator
  rm -rf .tmp
}

build() {
  if [ $TEST = false ]; then
    mkdocs build
  else
    mkdocs build --site-dir test
  fi
}

all() {
	fetch
	build
}

FRONTEND_REMOTE_REPO_DEFAULT='https://gitlab.com/soapbox-pub/soapbox.git'
LEGACY_REMOTE_REPO_DEFAULT='https://gitlab.com/soapbox-pub/soapbox-legacy.git'
MIGRATOR_REMOTE_REPO_DEFAULT='https://gitlab.com/soapbox-pub/migrator.git'

FRONTEND_LOCAL_REPO_DEFAULT='../soapbox'
LEGACY_LOCAL_REPO_DEFAULT='../soapbox-legacy'
MIGRATOR_LOCAL_REPO_DEFAULT='../migrator'

BRANCH='develop'

if [ -z "$1" ] || [ "$1" = "--help" ]; then
	echo "Usage: $(basename "$0") <stage> [<options>]

  The stages are:

  fetch [<options>]
    Fetch frontend and legacy documentation and dump it into \`docs\`.
    The location of frontend and legacy repositories defaults to
    $FRONTEND_REMOTE_REPO_DEFAULT and and can be overriden by \`--frontend-repo\`.
    The branch defaults to \`$BRANCH\` and can be overriden by \`--branch\`.

    If you want to use local copies of the repositories, add \`--local\`
    to options. Then the location of frontend, legacy, and migrator repositiories
    will default to $FRONTEND_LOCAL_REPO_DEFAULT, $LEGACY_REMOTE_REPO_DEFAULT,
    and $MIGRATOR_LOCAL_REPO_DEFAULT respectively and can be overriden by
    \`--frontend-repo\`, \`--legacy-repo\`, and \`--migrator-repo\` as well.

  build [<options>]
   Build the documentation

  all [<options>]
   Execute all stages
  "
else
  TEST=false
	LOCAL=false
        stage="$1"
	shift
	while echo "$1" | grep "^-" > /dev/null; do
		case "$1" in
		--local)
			LOCAL=true
			shift
			;;
		--frontend-repo)
			FRONTEND_REPO="$2"
			shift 2
			;;
    --legacy-repo)
  		LEGACY_REPO="$2"
  		shift 2
  		;;
    --migrator-repo)
      MIGRATOR_REPO="$2"
      shift 2
      ;;
		--branch)
			BRANCH="$2"
			shift 2
			;;
    --test)
      TEST=true
      shift
      ;;
		-*)
			echo "invalid option: $1" 1>&2
			shift 1
			;;
		esac
	done
	if [ $LOCAL = true ]; then
	  FRONTEND_REPO="${FRONTEND_REPO:-$FRONTEND_LOCAL_REPO_DEFAULT}"
	  LEGACY_REPO="${LEGACY_REPO:-$LEGACY_LOCAL_REPO_DEFAULT}"
	  MIGRATOR_REPO="${MIGRATOR_REPO:-$MIGRATOR_LOCAL_REPO_DEFAULT}"
	else
	  FRONTEND_REPO="${FRONTEND_REPO:-$FRONTEND_REMOTE_REPO_DEFAULT}"
	  LEGACY_REPO="${LEGACY_REPO:-$LEGACY_REMOTE_REPO_DEFAULT}"
	  MIGRATOR_REPO="${MIGRATOR_REPO:-$MIGRATOR_REMOTE_REPO_DEFAULT}"
	fi
	$stage
fi
